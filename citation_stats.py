
# coding: utf-8

# In[1]:


import glob
import matplotlib.pyplot as plt
import numpy as np


# In[2]:


file_name = glob.glob('./*.aux')


# In[3]:


bibcite_strings = []
with open(file_name[0]) as fp:
    line = fp.readline()
    cnt = 1
    while line:
#         print(line.strip())
        if 'bibcite' in line.strip():
            bibcite_strings.append(line.strip()[:-3])
        line = fp.readline()


# In[4]:


def get_years(bibcite_strings):
    years = []
    for str1 in bibcite_strings:
        IntVar = int("".join(filter(str.isdigit, str1)))
        if IntVar == 20124824:
            IntVar=2012
        elif IntVar == 20162:
            IntVar=2016
        elif IntVar==8416973:
            IntVar=2018
        years.append(IntVar)
    return years


# In[5]:


years = get_years(bibcite_strings)


# In[6]:


def count_elements(seq) -> dict:
    """Tally elements from `seq`."""
    hist = {}
    for i in seq:
        hist[i] = hist.get(i, 0) + 1
    return hist


# In[7]:


c = count_elements(years)
print(c)

# In[11]:


# %matplotlib inline
# An "interface" to matplotlib.axes.Axes.hist() method
plt.rc('axes', axisbelow=True)
plt.grid(linestyle='--')
n, bins, patches = plt.hist(x=np.array(years), bins=70)
plt.xlabel('Years')
plt.ylabel('# of citations')
plt.xticks(range(1950,2025, 10))
plt.savefig('citation_histogram.png', dpi=180)


# In[9]:


percentage = np.sum((bins[1:]>=2014)*n)/np.sum(n) *100


# In[10]:


print('percentage of citations since 2014: {}%'.format(percentage))
