\chapter{Results and Discussion}

\section{Introduction}

In this chapter, the effectiveness of the proposed method will be verified in a simulation environment and compared against the method developed by Long et al. \cite{Long2018}. This Chapter contains a description of the experiments conducted which includes the simulation environment, the training configuration and the hyperparameters used. This chapter also contains a description of the testing and evaluation of our work and both quantitative and qualitative comparison between our work and Long et al.'s work.

\section{Simulation Setup} \label{section:simulation_setup}

A simulation environment was developed to train and test our deep reinforcement learning method. The robots used were the Turtlebot3 Waffle PI robots \cite{turtlebot2018}. The simulation environment is an adaptation of the one built by the Turtlebot3 robot team which was modified to support multiple robots and goals positions. The simulation environment was built using the Gazebo simulator \cite{Koenig2004} shown in Figure~\ref{fig:simulation_env}. The Simulation environment is a $4m\times4m$ square environment surrounded by a fence. The robots and the goal positions are spawned inside the environment. The robots have fixed starting positions while the goals' positions are chosen randomly from a set of predefined positions. In the case of training, The set of predefined goals positions hold 33 positions while during testing it holds the same goal positions with 10 extra positions to test for generality. The goal positions were changed when each and every robot reaches a unique goal successfully or when a certain amount of episodes $\goalChange$ are passed since the last goal positions change.

\begin{figure}[h]
  \begin{centering}
    \framebox{\includegraphics[scale=0.5]{figs/stage1.png}}
  \par\end{centering}
  \caption{Simulation environment, The size of the environment is $4m\times4m$ and it is shown holding 2 robots with 2 goal positions.}
  \label{fig:simulation_env}       % Give a unique label
\end{figure}

\section{Multiple Environments}

During training, multiple simulation environments were run in parallel to leverage the ability to collect data representing the different experiences in each environment. Figure~\ref{fig:multi_env} shows an overview of the system, in which there are two main components: the rollout workers and the optimizer. Each of the rollout workers uses a docker container \cite{docker2014} that runs the simulation environment and has a local copy of both the value and policy networks that are used to collect the data needed for training. Once the collected data reaches a certain limit $\dataThresh$ then the data rollout is sent to the optimizer part. The optimizer part itself runs a docker container which contains an experience buffer and a centralized copy of the policy and value networks. The experience buffer collects the rollout data and once all the rollout workers have sent their data then the centralized copy of the policy and value networks are trained and then the new parameter values of each network are sent to the rollout workers to collect new data and repeat the cycle.

\begin{figure}[!htb]
  \begin{centering}
    \framebox{\includegraphics[scale=0.6]{figs/multi_env_crop.png}}
  \par\end{centering}
\caption{Multiple environments system overview.}
\label{fig:multi_env}       % Give a unique label
\end{figure}

\section{Training Configuration}

The algorithm was implemented using Tensorflow \cite{Abadi2016} on a NVIDIA DGX-1 station. The training took 94 hours (about 8k iterations). We used 10 rollout workers, the simulation in each one contained two robots $N=2$ with a total of 20 robots. The rollout workers were running the simulation in real time and the local policy and value networks were running on the CPU. While the optimizer was running on the GPU cluster. The frequency of computing the actions was set to $5 HZ$. Table \ref{tab:hyperparametrs} shows the hyperparameters used during training. The evolution of the reward over training is shown in Figure~\ref{fig:reward}.

\input{includes/training_config_table.tex}

\begin{figure}[!htb]
  \begin{centering}
    \framebox[\linewidth]{\includegraphics[scale=0.43]{figs/reward.png}}
  \par\end{centering}
\caption{Average of the rewards gained per episode averaged over all the robots in each worker environment plotted against training wall time.}
\label{fig:reward}       % Give a unique label
\end{figure}

\section{Evaluation and Testing}

We compared our method with the method proposed by Long et al. \cite{Long2018}. The reason we are comparing with their work is the similarity in the objective of the study in terms of performing navigation of multi robot systems using deep reinforcement learning. On the other hand, the difference between our method and the method proposed by Long et al. is that our policy selects which goal position to drive the robot towards, while in Long et al.'s method the policy needs the goal position to be preselected for each robot. Thus, our method performs MRTA and navigation from end to end while Long et al.'s method performs only navigation from end to end. The second reason the method proposed by Long et al. was chosen is because the performance metrics their paper reports, it reports better performance than the NH-ORCA method \cite{alonso-mora2013} over most test cases. We trained the method proposed by Long et al. on our simulation setup with the same robot kind we are using. Their method was implemented using the same setup, network architecture and hyperparameters that they proposed in their paper. The training process for their work took around $22$ hours.

\subsection{Evaluation Metrics}

Since the system is trained from end to end, and since the decisions made by a neural network are challenging to explain and decision explainability is still an open field of research \cite{Stoica2017}. It's challenging to make a distinction between the parts of the policy that performs MRTA, MPP and path tracking, a similar problem was faced by Bojarski et al. \cite{Bojarski2016} while training an end to end autonomous vehicle system that they couldn't make a clean break between the parts of the neural network that perform feature extraction and the parts that serve as the controller.

On the other hand, The overall performance of the policy can be evaluated using the same quantitative metrics used by Long et al. \cite{Long2018}:
\begin{itemize}
  \item Success rate: The ratio of the number of robots that reach unique goals successfully during the episode over the total number of robots. The higher the better.
  \item Extra time: The difference between the actual travel time of a robot to reach a goal position and the time it takes the robot to go to the same goal position in a straight line with maximum speed. The lower the better.
  \item Extra distance: The difference between the actual distance a robot traveled to reach a goal position and the Euclidean distance between the robot's start position and the goal position. The lower the better.
  \item Average speed: The average of the ratio of the speed of the robot team relative to the maximum speed of a robot during the episode.
\end{itemize}

The metrics are measured over all the robots and all episodes during evaluation to remove any effects of the variance in the goals positions.

We categorize the robot's collisions into three categories. Category A is a collision with an obstacle, category B is one robot colliding with another robot that reached a goal position and finally category C is two robots colliding together while moving in free space. Such categorization enabled us to develop a metric that can measure an end to end policy's performance in MRTA by measuring the policy's failure in performing it. We call this new metric task allocation index $\tai$. It depends on the ratio of episodes ending with category B collisions relative to the total number of episodes because category B collisions are a clear failure of MRTA when two robots reach the same goal. $\tai$ also depends on the ratio of goals whose information is input to the policy relative to the total number of goals in the environment as this gives a higher score for the policies that performs task allocation on a bigger number of goals. \Eq{eq:tai} shows the formula for $\tai$.

\begin{equation} \label{eq:tai}
  \tai = \dfrac{\inputGoalsToNN - 1}{N - 1}\times\brkt{1 - \dfrac{\NCateogryB}{\NEpisodes}}
\end{equation}

where:
\begin{itemize}
  \item $\inputGoalsToNN$ : Number of goals  whose information is input to the policy.
  \item $N$ : Total Number of goals.
  \item $\NCateogryB$ : Number of episodes ended by category B collisions.
  \item $\NEpisodes$ : Total number of episodes.
\end{itemize}

\subsection{Testing Description And Results}

To test and evaluate both methods, we repeated the evaluations for $2$ hours. The goals positions were changed every time each robot reaches a unique goal or 5 episodes pass by since the last goal position change $\goalChange = 5$. The results are shown in Table~\ref{tab:results}. In case of measuring the $\tai$, our method has a higher score since the number of episodes ending with category B collisions is low and Long et al.'s \cite{Long2018} method doesn't perform MRTA i.e. $\inputGoalsToNN = 1$, which according to \Eq{eq:tai} leaves it with a $\tai$ score of 0. On the other hand, there is a slight advantage of the method proposed by Long et al. over ours in terms of success rate and the time and distance it takes for a robot to reach a goal position. It is clear for the percentage of episodes ending with collisions that our method tends to collide more than Long et al.'s method by approximately four times, although both methods have a low collision rate. Our method has a tendency to move the robots with an average speed that is $1.44$ times the average speed produced by Long et al.'s method.

\begin{table}[!htb]
\caption{Performance metrics as (mean/std) for both methods during a 2 hour test.}
\label{tab:results}
\begin{tabular}{l c c}
\hline
 & \textbf{our method} & \textbf{Long et al.}  \\
\hline
$\tai$ & $\mathbf{0.925}$ & $0$ \\
Success rate & $0.948 / 0.159$ &  $0.983 / 0.116$ \\
Extra time (s) & $6.569 / 4.704$ & $5.985 / 5.043$ \\
Extra distance (m) & $2.280 / 1.135$ & $2.013 / 1.328$\\
Average speed & $0.613 / 0.194$  & $0.425 / 0.209$ \\
Number of episodes & $268$       & $402$ \\
\% of episodes ending by collision & $8.955 \%$ & $2.239 \%$\\
\% of episodes ending by exhausting episode time & $0.746 \%$ & $0.0 \%$\\
\hline
\end{tabular}
\end{table}

In Figure~\ref{fig:traj}, we visualize three different successful episodes for each method. The visualization shows the trajectory of each robot color coded where Robot 0 is red and Robot 1 is green. The opaqueness of the color shows the time progression, the more opaque the color of nodes on the trajectory the more advanced in the episode time it is. It is clear from the graph that Long et al.'s method tends to move in more straight lines while our method tends to move in trajectories that are more irregular and sometimes overshoots the goal positions.

\input{includes/traj_fig.tex}

Figure~\ref{fig:goal_dist} shows the distribution of goals reached by each robot throughout the environment during testing. It shows that Robot 0 which is spawned near the upper right corner of the environment tends to reach the goals positioned in the environment's upper half, while Robot 1 which is spawned in the lower left corner tends to reach the goals positioned in the environment's lower half. The past two observations indicate that the task allocation strategy learned during training is to split the environment into two halves and each robot attempts to reach the goals available in its half.

\input{includes/reaching_goals_dist.tex}

\subsection{Collision Analysis}

To analyze the collisions performed by each method, Figure~\ref{fig:col_pie} shows the percentage of occurrence of each category during testing. It shows that both methods managed to avoid any collisions of category C, while our method tends to have collisions of category B about five times more than that of category A which indicates that the policies on both robots chose the same goal position to move towards. Samples of both categories of collisions are visualized in Figure~\ref{fig:col_traj}.

\input{includes/collisions_pie_chart.tex}

\input{includes/collision_traj_fig.tex}

On the other hand, Long et al.'s \cite{Long2018} method tends to have slightly more collisions of category A than that of category B. Most of the category A collisions occur when the goal position is near the obstacle wall which means that the policy drives the robot to approach such goals in an angle that exposes it to collision. Alternatively, The category B collisions occur when the other robot reaches a goal position that is in the middle between the ego robot and it's assigned goal position as shown in the last row in Figure~\ref{fig:col_traj}, which indicates that the policy failed to avoid a stationary robot in front if it. Figure~\ref{fig:col_dist} shows the distribution of collisions within the environment, it shows that for our method the collisions are concentrated in the middle of the environment and such collisions are of category B. On the other hand for Long et al.'s method, most collisions are near the edges of the environment which and such collisions are of category A.

\input{includes/collision_dist.tex}
