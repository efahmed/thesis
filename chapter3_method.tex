\chapter{Methodology}

\setcounter{algorithm}{0}
\section{Introduction}

In the previous chapter, the most common used algorithms for multiple mobile robot systems and the state of the art of using deep reinforcement learning in such systems were described. The previous chapter emphasized that so far there has been little discussion in the literature about using DRL to develop and end to end system that performs all MRTA, MPP and path tracking.

This chapter describes the method proposed to overcome the highlighted limitations. This chapter contains a detailed description of the proposed DRL agent whose policy is in the form of a deep neural network. The policy performs decentralized multi robot task assignment (MRTA), multi robot path planning (MPP), path tracking and motor controls. The inputs to the policy are the positions of the goals, the positions of the other robots and the sensors' readings of the ego-robot \cite{Gromniak2019}\cite{Wang2020} on which the policy is running. The outputs of the policy are the velocities of the ego-robot. The output of the network should drive the ego-robot towards a unique goal that none of the other robots are moving towards while avoiding the obstacles present in the environment. This chapter also contains a detailed description of the reward function that drives the agent to behave in the desired manner.

\section{Problem Formulation} \label{section:prob_form}

The problem of multi-robot task allocation and navigation is defined in the context of a differential drive mobile robot moving in a 2D plane with obstacles and other moving robots (all robots are homogeneous). Each robot has a $\ang{360}$ laser scanner mounted on it. The goal locations are laid out in the environment. The number of goal locations equals the number of robots and is denoted by $N$.

Each robot $i$---where $\brkt{1 \leq i \leq N}$---at time step $t$ receives an observation $\obsv$ and calculates the output command $\act{t}$ which drives the robot from the start position $P_i$ towards the goal position $G_i$. The observation is drawn from a probability distribution $p\left( \obsv|s_i^t, \act{t}\right)$ which depends on the underlying system state and previous action. The observation for each robot is composed of four parts: $\obsv = \left[ \obsvEgo\, , \obsvOthr\, , \obsvLzr\, , \obsvAux \right]$, where $\obsvEgo$ is the relative positions of all the goals in the ego-robot's local polar coordinates, $\obsvOthr$ is the relative positions of all the goals in the other moving robots' local polar coordinates, $\obsvLzr$ is the 2D laser scanner measurements, while $\obsvAux$ is a vector containing the current time of the robot starting from the beginning of the robot's motion and the robot's previous actions $\act{t-1}$. The observation only provides partial information about the robot's state and the other robots' states, which leads the robot to predict the other robots' intent.

Given the observations, each robot calculates its own action independently. The action is composed of two parts: $\act{t}=[\LV{t}\ , \RV{t}]$, where $\LV{t}$ is the robot's linear velocity and $\RV{t}$ is the robot's rotational velocity. \Eq{eq:action_sampling} shows that the actions are sampled from the distributions produced by the policy $(\pi)$ whose parameters are denoted by $\theta$ which is copied across all the robots.

\begin{equation} \label{eq:action_sampling}
a_i^t \sim \pi_\theta(a_i^t|\obsv)
\end{equation}

The output action guides the robot closer towards the goal which the policy selected while avoiding collisions on the way to the goal within the time limit between two observations. The target allocation is done in a way that the policy doesn't explicitly select one of the goals, but outputs the actions that drive the robot towards the selected goal position.

The problem is formulated as a partially observable markov decision process (POMDP) \cite{spaan2012}. In which, a sequence of observations and actions made by each robot forms a trajectory $\tau_i$ from its start position $\robPos^0$ till the goal position chosen by the policy $g_i$ and $t_i^g$ is the travelled time of a trajectory. The POMDP is episodic in which each episode ends either by one of the robots having a collision, all the robots successfully reach unique goals' positions or the maximum episode duration $\maxTime$ is exhausted.

The main objective is to find an optimal policy that successfully drives each robot to a unique goal position. A secondary objective for this optimal policy is to minimize the expectation of the total arrival time of all robots to their goals as defined in \Eq{eq:expectation}.

\begin{equation} \label{eq:expectation}
  \operatorname*{argmin}_{\pi_\theta} \left(E \left[ \operatorname*{argmax}_i \left(t_i^g|\pi_\theta \right) \right]\right)
\end{equation}

\section{Deep Reinforcement Learning Setup}

Within this deep reinforcement learning setup, there is a focus on learning a policy that is capable of controlling a robot in a decentralized manner within a multi robot system. The policy selects one goal position within multiple ones and produces a series of actions that gets the robot to the goal position while avoiding collisions. Inherently the policy should learn the intents of other robots and which goal position each of the other robots is going towards and aim for a different goal position. The policy learns by going through episodes of collecting data and updating its parameters in order to increase the collected reward.

\subsection{Observation Description} \label{section:observation}

The observation space---as mentioned in Section \ref{section:prob_form}---is consisted of $\obsv = \left[ \obsvEgo\, , \obsvOthr\, , \obsvLzr\, , \obsvAux \right]$. $\obsvEgo$ is the relative goals positions in the ego-robot's polar coordinates. $\obsvEgo$ is a 3D tensor $\realSpace{\obsvEgo}{q \times b \times N}$. The first dimension $q$ represents the values of the past consecutive number of frames of this observation. The second dimension $b$ represents a goal's distance and heading from the ego-robot in the ego-robot's polar coordinates, thus $b=2$. The third dimension $N$ is the number of goals in the environment. Since $\obsvEgo$ encompasses the information of all the goals relative to the ego-robot, it enables the robot to choose which goal position to aim towards. While $\obsvOthr$ is the relative goals' positions in the other robots polar coordinates. $\obsvOthr$ is a 4D tensor $\realSpace{\obsvOthr}{N-1 \times q \times b \times N}$. The first dimension is the number of the other robots $N-1$ and the other dimensions are similar to the dimensions of $\obsvEgo$. Since $\obsvOthr$ encompasses the information of all the goals relative to the other robots, it enables the ego-robot to predict the intents of the other robots and which robot is aiming towards which goal, this aids the ego-robot to choose a unique goal position to aim towards. $\obsvLzr$ is the $\ang{360}$ laser scanner data of the ego-robot. It's a 2D tensor $\realSpace{\obsvLzr}{q \times 360}$. The second dimension represents all 360 distance values. $\obsvLzr$ enables the robot to detect obstacles and helps the robot to navigate without collisions. Finally $\obsvAux$ is a 1D vector $\realSpace{\obsvAux}{3}$ representing the time spent since the ego-robot started to move and the previous action which consists of: the linear velocity $\LV{t-1}$ and rotational velocity $\RV{t-1}$ of the ego-robot at the last time step. Since the policy is required to train to minimize the expectation of the total arrival time of all robots to their goals successfully, the policy on each robot requires an input that informs it of the time it's spending while moving. Having the previous action as an input informs the robot about its intent in the last time step.

\subsection{Actions Description} \label{section:action_desc}

The action space $\realSpace{\act{t}}{2}$ is consisted of two parts: the robot's linear velocity $\LV{t}$ and the robot's rotational velocity $\RV{t}$ where $\act{t} = \left[\LV{t}\, , \RV{t} \right]$. The policy network has two output heads $\brkt{\headNN{\LV{t}}\, , \headNN{\RV{t}}}$ each of them is a softmax distribution \cite{sutton2017} that represents $\brkt{\LV{t}\, , \RV{t}}$ respectively. Each softmax distribution is a $K$ dimensional categorical distribution $\realSpace{\headNN{\LV{t}}\, , \headNN{\RV{t}}} {K}$. The category with the highest probability is converted to a continuous value within the range [-1,1] for each action. Equation (\ref{eq:disc2cont}) is used for the conversion, it is shown applied on $\LV{t}$ but its also applicable on $\RV{t}$.

\begin{equation} \label{eq:disc2cont}
  \LV{t} = \dfrac{1}{K}\brkt{2\argmaxB\brkt{\headNN{\LV{t}}} + 1} - 1
\end{equation}

\subsection{Reward Function} \label{section:reward}

The reward function is designed to incentivize each robot to choose and move towards a unique goal position while minimizing the time consumed to reach the goal position. It penalizes collisions with obstacles or other robots and penalizes multiple robots reaching the same goal position. Equation (\ref{eq:reward}) shows the reward $r$ received by robot $i$ at time step $t$.

\begin{equation} \label{eq:reward}
  \rwrd = \Drwrd + \OBrwrd + \OTHrwrd + \Trwrd
\end{equation}

Where $\Drwrd$ is used to incentivize the ego-robot to go towards a goal position. It is calculated in Equation (\ref{eq:distance_reward}) using the matrix $\realSpace{G}{2\times N}$ which contains the positions of all of the goals. The first row of $G$ describes the $x$ position of each goal while the second row describes the $y$ position, the number of columns is the number of the goal positions $N$. The robot position vector at time step $t$ is described as $\realSpace{\robPos^t}{2}$. The ego-robot receives a positive reward value $\ARRIVrwrd$ if it reaches the goal position otherwise it's either rewarded or penalized if it moves towards or away from the nearest goal position $\brkt{\robGoalDist{t}}$. All the distances are normalized by dividing their values by the largest distance in the environment i.e.\ the environment's diagonal.

% \begin{equation} \label{eq:distance_diff}
%   \distDiff = \dfrac{\robGoalDist{t} - \robGoalDist{t-1}}{dt}
% \end{equation}

\begin{equation} \label{eq:distance_reward}
  \Drwrd =
    \begin{cases}
      \ARRIVrwrd & ,\ if\ \robGoalDist{t} < 0.2\\
      \distCoeff\distDiff &,\ otherwise\\
    \end{cases}
\end{equation}

Where $\distDiff = \brkt{\robGoalDist{t} - \robGoalDist{t-1}}\mathbin{/}dt$.

 Equation (\ref{eq:obstacle_reward}) describes $\OBrwrd$ which is the reward function that penalizes the ego-robot in case of collisions. The collisions with obstacles are detected using the 2D laser scanner readings on the ego-robot $\realSpace{\lzrScan}{360}$. The collisions with other robots are detected by thresholding the distance between the ego-robot and other robots. The distance with other robots is calculated using the matrix $\realSpace{\otherRobPos^t}{2 \times N-1}$ which describes the positions of the other robots at time step $t$. In case if a collision is detected, the ego-robot receives a negative reward value $-\ARRIVrwrd$.

% \begin{equation} \label{eq:condition1}
%   cond_1 = \min{\brkt{\lzrScan}} < 1.1\turtBotR
% \end{equation}
%
% \begin{equation} \label{eq:condition2}
%   cond_2 = \otherRobDist{t} \leq 2\turtBotR
% \end{equation}

\begin{equation} \label{eq:obstacle_reward}
  \OBrwrd =
  \begin{cases}
    -\ARRIVrwrd & ,\ if\ cond_1\ or\ cond_2 \\
    0 &,\ otherwise\\
  \end{cases}
\end{equation}

Where $cond_1$ is $\min{\brkt{\lzrScan}} < 1.1\turtBotR$, $cond_2$ is $\otherRobDist{t} \leq 2\turtBotR$ and $\turtBotR$ is the radius around the robot in which a collision is registered.

\Eq{eq:other_robot_reward} describes $\OTHrwrd$ which rewards or penalizes The ego-robot if it is moving towards a unique goal position or towards a goal position that another robot is moving towards.

% \begin{equation} \label{eq:nearst_other_robot_reward}
%   \nearstOthrRobVal = \norm{\nearstOthrRob{t}}
% \end{equation}

\begin{equation} \label{eq:other_robot_reward}
  \OTHrwrd = \othrRobCoef \log_{10}\brkt{\min{\brkt{\nearstOthrRobVal}}} + \othrRobBias
\end{equation}

Where $\nearstOthrRobVal = \norm{\nearstOthrRob{t}}$.

Finally, \Eq{eq:time_reward} describes $\Trwrd$ which penalizes the ego-robot depending the amount of time it takes to reach a goal position. The ego-robot receives a negative reward $-\ARRIVrwrd$ if the maximum episode duration $\maxTime$ is exhausted before reaching a goal position.

\begin{equation} \label{eq:time_reward}
  \Trwrd =
  \begin{cases}
    -\ARRIVrwrd &,\ if\ t \geq \maxTime\\
    \timeCoef\frac{t}{\maxTime} &,\ otherwise\\
  \end{cases}
\end{equation}

All the constants $\ARRIVrwrd,\ \distCoeff,\ \othrRobCoef,\ \othrRobBias,\ \maxTime$ and $\timeCoef$ are set during experimentation, the constant $\turtBotR$ depends on the robot dimensions.

\subsection{Training Algorithm} \label{section:training_alg}

The deep reinforcement learning algorithm used is the Proximal Policy Optimization (PPO) algorithm \cite{Schulman2017} with the actor-critic setup. We are adapting the centralized learning, decentralized execution paradigm \cite{Long2018} in which each robot has a copy of the policy $\policy{}$ and the value $\valueN$ networks. Both the policy and the value networks have different sets of weights $\policyW$ and $\criticW$ respectively.

Algorithm~\ref{alg:ppo} summarizes the entire data collection and training process. At each time step, each robot receives its observation $\obsv$ and uses its copy of the policy $\policy{}$ to generate the action $\act{t}$. Each robot collects its data $\brkt{\obsv,\act{t},\rwrd}$ from the environment and after the amount of data exceeds a certain threshold $\dataThresh$, it sends the rollouts of data over to a centralized copy of both the policy and the value networks. Then the gradients of the objective function $\totalObj$ with respect to the centralized policy network weights $\policyW$ and value network weights $\criticW$ are computed. Then the Adam optimizer \cite{kingma2015} updates the weights $\policyW$ and $\criticW$ using the learning rate $\learningRate$ for $\epochs$ epochs. After each update, each robot receives a copy of the updated weights $\policyW$ and $\criticW$ to start collecting a new batch of data. Thus, the policy and value networks are trained on the experiences collected by all the robots simultaneously.

\input{includes/ppo_alg.tex}
\clearpage

As shown in Algorithm~\ref{alg:ppo}, line~\ref{line:toal_objective}. The objective function $\totalObj$ is a combination of three objective functions. The first one is the PPO clipped surrogate objective function $\clipSurrObj$ ---explained in section \ref{section:ppo}---with the generalized advantage function (GAE) \cite{SchulmanMLJA2016} $\gae{}$ as the advantage estimator. The GAE $\gae{}$ used the value estimation obtained from the value network $\valueN \brkt{\obsv}$ as the a baseline and $\lambda$ as the decay parameter---as explained in section \ref{section:return_value}. The PPO clipped surrogate objective function $\clipSurrObj$ is used to update the policy network. The second objective function $\valueObj$ is used to train the value network $\valueN$ to reduce the mean squared error between the returns $\returns{}$ and the value estimation $\valueN \brkt{\obsv}$. The third objective function is the entropy $\entropyObj$ of the policy which is used to ensure that the policy performs sufficient environment exploration over the training period. Gradient ascent is performed on the objective function by performing gradient descent on its negative value $-\totalObj$.
% \input{includes/training_diagram.tex}

\subsection{Network Architecture}

\input{includes/nn_arch.tex}

The policy network $\policy{}$ takes the observation $\obsv$ as the input and outputs the action $\act{t}$. Figure \ref{fig:policy_arch} shows the architecture of the policy network. There are four inputs to the policy network each one corresponds to one of the observation components $\obsv = \left[ \obsvEgo\, , \obsvOthr\, , \obsvLzr\, , \obsvAux \right]$. Each input has its branch of hidden layers that it passes through. The input $\obsvLzr$ goes through the first branch, it passes through a convolutional layer \cite{lecun2015} that consists of 32 one-dimensional filters with a kernel size of 5 and a stride of 2. The second hidden layer in the branch is another convolutional layer similar to the first one except it has a kernel size of 3. The third hidden layer is a flatten layer followed by a 128 neuron fully-connected layer \cite{lecun2015}. The second branch that takes the input $\obsvOthr$ passes it through a reshape layer that reshapes it into the shape $\real{N-1 \times \brkt{q \times b \times N}}$ then it's passed through a 32 neuron fully-connected layer applied on the last dimension of the reshape layer output. Then the output of the fully connected layer is flattened. The observation $\obsvEgo$ goes through the third branch which is just one flatten layer. The fourth branch has no hidden layers as it passes the $\obsvAux$ input through to the next stage. The outputs of all the branches are then concatenated then passed through a 256 neuron fully-connected layer. All the layers until now have ReLU activations \cite{hinton2010}. The final layers are the two output layers each corresponding to an action component  $\act{t} = \left[\LV{t}\, , \RV{t} \right]$. Each layer has K neurons with softmax activations \cite{martins2016}. The components of the action $\act{t}$ are then sampled from the two softmax distributions as explained in section \ref{section:action_desc}.


The value network $\valueN$ shown in Figure \ref{fig:critic_arch} shares the same architecture with the policy network except for the outputs. The output layer of the value network outputs the network's prediction of the value estimation and it is a single neuron fully connected layer with no activations. The value network has a separate set of weights than the policy network.

\input{includes/critic_arch.tex}
%gonna need the number of parameters of the layer
