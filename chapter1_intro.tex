\chapter{Introduction}

\section{Background}\label{01_section:background}

% Multi-robot systems are designed to achieve tasks by collaboration.
% Multi-robot systems (MRS) are a group of robots that are designed aiming to perform some collective behavior
% reliability , complex problem, simplicity of design , increase of productivity
%sequential decision making
Multi robot systems (MRS) are systems composed of multiple robots that are designed to cooperate together to achieve a given task. There are numerous benefits of such systems over single robot systems. The first of such benefits is the ability of a multi robot system to solve a more complex task than a single robot system as the nature of such task can be composed of multiple distributed subtasks. A second benefit is the increase of productivity in a way that multiple robots can collaborate to perform the required task causing a reduction in task completion time. Another benefit of using multi robot systems over single robot systems is the inherent reliability of the system due to the redundancy of having multiple robots. An added benefit is the ability of the multi robot systems to use simpler robots to perform the same task than single robot systems.

The use of multi robot systems has been adopted in multiple areas and fields of industry. Ranging from intelligent security to search and rescue, but the field that was disrupted the most by the introduction of multi robot systems is the pick-pack-and-ship warehousing industry. Multi Robot systems were introduced in the form of robots that drive shelving units to workers who pick items from the shelve and place it into boxes ready for shipping. In the year 2008, it was estimated that using a multi robot system in a single warehouse would lead to an increase in productivity by a factor of two and reduced costs by 2 million USD per year \cite{Wurman2008}.

The field of multi robot systems have been studied over the years \cite{parker2008} and many different techniques and algorithms have been developed \cite{khamis2015} \cite{Opfer2011}. It is clear from the literature that there are three main algorithms used in a multi robot system: an algorithm that performs multi robot task allocation (MRTA), an algorithm that performs multi robot path planning (MPP) and an algorithm that performs path tracking as shown in Figure \ref{fig:mrs_algorithms}. MRTA is the algorithm that assigns robots to tasks in a multi robot system in order to decrease the probability of collisions between the robots and decrease the total cost expended by each robot, make it be: time, energy or distance \cite{khamis2015}. After each robot is assigned to a task, the MPP algorithm tries to find an optimal path for each robot from the start position to the task's position without colliding with any of the other robots or colliding with obstacles present in the environment \cite{Opfer2011} \cite{yu2016}. After a path is determined for each robot, a path tracking algorithm on each robot controls and moves the robot along its path \cite{Lundgren2003}. However, each algorithm suffers from a limitation that prevents it from being universally adopted and leaves the door open for more research in the area.
% The field of multi robot path planning has been extensively studied for years and many different techniques and algorithms have been developed \cite{Yu2016}. However, each algorithm suffers from a limitation that prevents it from being universally adopted and leaves the door open for more research in the area.\cite{Khamis2015}
\input{includes/mrs_algorithms_diagram.tex}
In recent years, the field of deep learning has seen increasingly rapid advances which revolutionized some fields like computer vision \cite{NIPS2012_4824} and natural language processing \cite{Young8416973}. In 2015 a breakthrough occurred when Minh et al. \cite{Mnih2015} used a deep convolutional neural network as a reinforcement learning agent and trained it on Atari 2600 games. The model received only the pixels and game scores as inputs and was able to perform much better than previous algorithms, opening the door to the new field of deep reinforcement learning (DRL). These promising results provided an opportunity for robotics researchers to investigate the use of DRL in the robotics domain, which led to advances in some robotics sub-fields like robotic manipulation \cite{Gu2017} and mobile robot navigation \cite{tongoly2017}. DRL was also used to perform both MPP and path tracking in a multi robot system \cite{Long2018}.

\section{Problem Statement} \label{section:prob_statement}

In a typical MRS cycle, First MRTA is used to assign each robot to a task then MPP is used to draw a path for each robot to it's task then path tracking ensures that each robot follows it's path. But in the case of an unknown environment or a dynamic environment, the MRS cycle starts and after the robots move and discover any obstacles in the environment, both the MRTA and MPP algorithms should be able to adapt to these changes. There has been little attention given in research to adapt MRTA and MPP to changes in the environment \cite{khamis2015} \cite{woosley2013}. Most MRS treat MRTA and MPP as two different steps \cite{hussein2014}. However, one of the main problems that occur due to the decoupling of both MRTA and MPP is that most MRTA algorithms lack incorporation of any dynamic changes in the path's cost due to the existence of unknown obstacles \cite{woosley2013} or due to partial observability.

Recently, researchers have been able to successfully incorporate DRL algorithms into MRS. For MRTA, multiple approaches have been using DRL to solve the general problem of multi agent resource allocation. For example, Zhu et al. \cite{zhu2018} treated the robots as resources that are requested to solve the tasks, then they used a DRL model to learn optimal strategies to select a resource requester. On the other hand, Chen et al. \cite{chen2017} has managed to build a value network that navigates two robots while avoiding collisions with each other. while Long et al. \cite{Long2018} has managed to train a DRL model for decentralized multi robot collision avoidance that only takes the sensor readings and the position of the robot and the goal and outputs both the linear and rotational velocity of the robots as actions. Effectively both Chen et al. \cite{chen2017} and Long et al. \cite{Long2018} have managed to build a deep neural network that performs both MPP and path tracking. As per our knowledge, an attempt to provide an end to end solution to a MRS which solves MRTA, MPP and path tracking have not been yet introduced.

\section{Research Objectives}

Based on the problems stated in Section \ref{section:prob_statement}, the following are the research objectives of this study:
\begin{enumerate}
\item To propose a deep reinforcement learning model that solves MRTA, MPP and path tracking in an end to end fashion.
\item To train and evaluate the DRL model in a simple simulation environment that proves it's ability to learn to solve MRTA, MPP and path tracking.
\item To evaluate the performance of the proposed model and compare it with an established method in the literature.
\end{enumerate}

\section{Research Scope}

The following points represent the scope of this study:
\begin{itemize}
\item The robots are differential drive mobile wheeled robots.
\item The robots are homogeneous.
\item The robots are assumed to have enough power to finish the task, power management of the robots is not considered.
\item The location of the targets is assumed to be known, target discovery is not covered in this study.
\item The size of the simulation environment in which the robots are moving in has an area of $4 m\times4 m$.
% \item The number of tasks is equal to the number of robots.
\item Each robot is able to solve a single task at a time, each task requires one robot to perform it and the number of tasks is equal to the number of robots. After a task is performed it's not replaced with a new task. Thus the MRTA Problem is classified as a ST-SR-IA problem \cite{Gerkey2003}.
\end{itemize}

\section{Significance of Study}

The main significance of this study is the development of a deep reinforcement learning model that performs MRTA, MPP and path tracking in an end to end manner.

\section{Thesis Outline}

This thesis is divided into five chapters in total. Chapter 1 is an introductory chapter where the formal problem statement, the objectives of the study, the scope within which the study is conducted and the significance of the study are laid out.

Chapter 2 is a literature review related to the study. The chapter mainly discusses the common methods used for MRTA, MPP and path tracking. The chapter also discusses the state of the common methods used to train deep reinforcement learning agents and the applications of deep reinforcement learning in MRS.

Chapter 3 lays out the underlying idea of using deep reinforcement learning to combine MRTA, MPP and path tracking. Then the deep reinforcement learning model is proposed along with the description of the neural networks': inputs, outputs, reward functions used and the networks' architecture. 

Chapter 4 describes the experimentation conducted to train and test the deep reinforcement learning model. It describes the simulation environment and the hyperparameters used. The experimental results are presented, discussed and compared with results obtained from implementing an established method.

At the end, chapter 5 summarizes the entire thesis and concludes the study along with suggestions for future work to improve the proposed method.
